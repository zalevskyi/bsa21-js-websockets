Binary Studio Academy 2021

# Homework **HTTP, REST / WS**

## Installation

`npm install`

`npx tsc` - to build typescript in ./dist

`./cpnonts.ps1` - to copy not \*.ts files into ./dist

`npm start` - to run node

`npm dev` - to run nodemon

open http://localhost:3002/
