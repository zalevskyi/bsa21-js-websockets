# Usage: ./cpnonts.ps1 [-source directory | -destination directory]
param ($src = './src', $dist = './dist')
write-host "Copying (exlcuding .ts) $src/* -> $dist"
cp $src/* $dist -Force -Recurse -Exclude *.ts
write-host "Done"
