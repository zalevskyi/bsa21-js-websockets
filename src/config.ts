import path from 'path';

export const STATIC_PATH = path.join(getServerRoot(), 'public');
export const HTML_FILES_PATH = path.join(STATIC_PATH, 'html');

export const PORT = 3002;

function getServerRoot() {
  if (process.platform === 'win32') {
    return path.dirname(new URL(import.meta.url).pathname).slice(1);
  }
  return path.dirname(new URL(import.meta.url).pathname);
}
