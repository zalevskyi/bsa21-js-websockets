import { Server, Socket } from 'socket.io';
import { texts } from '../data.js';
import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from './config.js';

interface UserInRoom {
  name: string;
  ready: boolean;
  doneCount: number;
}

export class GameRoom {
  users: Map<string, UserInRoom>;
  name: string;
  server: Server;
  protected text: string = '';
  constructor(server: Server, name: string) {
    this.name = name;
    this.server = server;
    this.users = new Map();
  }
  add(username: string, socket: Socket) {
    if (this.users.has(socket.id)) {
      return false;
    } else {
      socket.join(this.name);
      this.users.set(socket.id, { name: username, ready: false, doneCount: 0 });
      this.server.to(this.name).emit('PLAYERS', this.getUsers());
      return true;
    }
  }
  delete(socketId: string) {
    this.users.delete(socketId);
    this.attemptStart();
  }
  getUsers() {
    const names: [string, boolean, number, number][] = [];
    this.users.forEach(data =>
      names.push([
        data.name,
        data.ready,
        data.doneCount,
        this.text.length === 0 ? 1 : this.text.length,
      ]),
    );
    return names;
  }
  setReady(socketId: string, ready: boolean) {
    const user = this.users.get(socketId);
    if (user) {
      user.ready = ready;
      this.server.to(this.name).emit('PLAYERS', this.getUsers());
      this.attemptStart();
    }
  }
  protected attemptStart() {
    for (let user of this.users.values()) {
      if (user.ready === false) {
        return;
      }
    }
    let textId = selectTextId();
    this.text = texts[textId] || '';
    this.server.to(this.name).emit('TEXT_SELECTED', textId);
    this.countdown();
  }
  protected countdown() {
    let secondsLeft = SECONDS_TIMER_BEFORE_START_GAME;
    const secondsPerTick = 1;
    this.server.to(this.name).emit('COUNTDOWN', secondsLeft);

    const counter = () => {
      secondsLeft -= secondsPerTick;
      this.server.to(this.name).emit('COUNTDOWN', secondsLeft);
      if (secondsLeft === 0) {
        clearInterval(timer);
        this.update();
      }
    };
    const timer = setInterval(counter, secondsPerTick * 1000);
  }
  protected update() {
    let secondsLeft = SECONDS_FOR_GAME;
    const secondsPerTick = 0.5;
    this.server.to(this.name).emit('LEFT', secondsLeft);
    const counter = () => {
      secondsLeft -= secondsPerTick;
      this.server.to(this.name).emit('LEFT', Math.round(secondsLeft));
      this.server.to(this.name).emit('PLAYERS', this.getUsers());
      if (secondsLeft === 0) {
        clearInterval(timer);
        this.update();
      }
    };
    const timer = setInterval(counter, secondsPerTick * 1000);
  }
  checkType(socketId: string, key: string) {
    const user = this.users.get(socketId);
    if (user) {
      if (key.length === 1) {
        if (key === this.text[user.doneCount]) {
          user.doneCount += 1;
        }
        this.server
          .to(socketId)
          .emit('TYPE_RESULT', [user.doneCount, this.text.length]);
      }
    }
  }
}

function selectTextId(): number {
  return Math.floor(Math.random() * texts.length);
}
