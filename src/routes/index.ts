import express from 'express';
import gameRoutes from './gameRoutes.js';
import loginRoutes from './loginRoutes.js';

export default (app: express.Application) => {
  app.use('/game', gameRoutes);
  app.use('/login', loginRoutes);
  app.use(
    '*',
    express.Router().get('*', (_req, res) => {
      res.redirect('/login');
    }),
  );
};
